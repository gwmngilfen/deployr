# DeployR

An RStudio addin to enable simple deployment of an
RMarkdown report or Shiny app, via Rsync and SSH. In
other words, a cheap version of RSConnect :)

# Assumptions

The addin assumes that SSH to the remote host will work,
that the target directory is writable, etc. In other words,
that if you `rsync` for yourself, it'd work.

This means you're responsible for access (SSH keys), directory
creation, etc.

# Installation

Not available on CRAN, but you can get it via the `remotes`
package:

```
remotes::install_gitlab('gwmngilfen/deployr')
```

# Usage

Since the target deployment of a given project changes
fairly infrequently, it's sensible to record project-specific
defaults, and keep the actual execution to a minimum.

So, `deployR` uses a YAML config file in the project root,
namely `.deployr.yml`. This has only a few variables, and the
defaults are shown here:

```
host: localhost
source: /path/to/current/project
target: /tmp/project
delete: false
restart_shiny: false
```

By default, DeployR copies the current project to `/tmp` on the target server -
in other words, it would result in a copy of `project` being placed in
`localhost:/tmp/project`. The copy excludes `.git`, `.gitignore` and any
files/dirs listed in `.gitignore`. You can execute it from the Addins menu, or
by calling `deployr::deploy()` in the R shell.

If `delete` is `TRUE` then DeployR will include the `--delete` flag which will
delete *all* files which are not in the source tree, or ignored by exclude
directives in `.gitignore`.

If `restart_shiny` is `TRUE` then DeployR will also touch the `restart.txt` file
in the project directory, causing Shiny Server to reload the app.

# Caveats

There is no warning prompt by default - *please* test your config! You can use
the `debug = TRUE` parameter to perform a dry-run of the config (i.e. call
`deployr::deploy(debug = TRUE)` which will output the YAML config, the rsync
command, and the result of a dry-run of rsync.

(This will be improved, but for an alpha, it'll have to do for now)

# Extending Rsync

If you need to pass further options to Rsync, you can use the `extra_rsync`
option. For example, to make backups of modified files, you could call
`deployr::deploy(extra_rsync='-b --suffix=.bak')`

# TODOs

* Better handling of debug / testing YAML config for users
* Allow overriding YAML options in the function args
  * Use ... and match names?
* Add an RStudio Gadget UI for generating the YML file
  * Could also create the target dir on the server, and test config
* Add another Gadget UI as a confirm check when deploying
