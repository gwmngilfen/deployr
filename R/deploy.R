#' Launch an Rsync / SSH connection to copy the current project
#' to a remote location
#'
#' The source dir, remote host, and target dir are stored in a
#' project-specific YAML file (\code{.deployr.yml}), see README
#'
#' @param debug boolean; should we print debug output and only dry-run rsync
#' @param extra_rsync character; extra options to pass to the rsync command
#' @export
#' @examples
#' \dontrun{
#' deployr::deploy()
#' }

deploy <- function(debug = FALSE, extra_rsync = ''){
  opts <- read_opts()
  if (debug) print(opts)

  # Set up the command string
  command = paste0('rsync -avx --exclude-from .gitignore ',
                   '--exclude .gitignore ',
                   '--exclude .git ',
                   ifelse(opts$delete, '--delete ', ''),
                   ifelse(debug, '--dry-run ', ''),
                   extra_rsync, ' ',
                   '. $HOST:$TARGET')

  if (debug) print(command)

  # save a process ID
  terminalId <- rstudioapi::terminalExecute(
    workingDir = opts$source,
    env = c(
      paste0('HOST=',   opts$host),
      paste0('TARGET=', opts$target)
    ),
    command = command,
    show = FALSE
    )

  while (is.null(rstudioapi::terminalExitCode(terminalId))) {
    Sys.sleep(0.1)
  }

  result <- rstudioapi::terminalBuffer(terminalId)
  rstudioapi::terminalKill(terminalId)
  print(result)

  if (opts$restart_shiny) {
    restart_out <- touch_restart()
    result <- c(result, restart_out)
  }

  invisible(result)
}

#' Launch an Rsync / SSH connection to touch `restart.txt`
#' on the remote location
#'
#' The source dir, remote host, and target dir are stored in a
#' project-specific YAML file (\code{.deployr.yml}), see README
#' @export
#' @examples
#' \dontrun{
#' deployr::touch_restart()
#' }

touch_restart <- function() {
  opts <- read_opts()

  # Might have rsync-access only, not shell access,
  # so touch a local file and copy it to the server

  writeLines('','/tmp/restart.txt')

  command = 'rsync -v /tmp/restart.txt $HOST:$TARGET/restart.txt'

  # save a process ID
  terminalId <- rstudioapi::terminalExecute(
    workingDir = '/tmp',
    env = c(
      paste0('HOST=',   opts$host),
      paste0('TARGET=', opts$target)
    ),
    command = command,
    show = FALSE
  )

  while (is.null(rstudioapi::terminalExitCode(terminalId))) {
    Sys.sleep(0.1)
  }

  result <- rstudioapi::terminalBuffer(terminalId)
  rstudioapi::terminalKill(terminalId)
  invisible(result)
}

# Read the DeployR options file
# Returns defaults if unset

read_opts <- function() {
  yml <- if (file.exists('.deployr.yml')) {
    yaml::yaml.load_file('.deployr.yml')
  } else {
    list()
  }

  if (is.null(yml$host))          { yml$host   <- 'localhost' }
  if (is.null(yml$source))        { yml$source <- getwd() }
  if (is.null(yml$target)) {
    yml$target <- paste0('/tmp',basename(getwd()))
  }
  if (is.null(yml$delete))        { yml$delete <- FALSE }
  if (is.null(yml$restart_shiny)) { yml$restart_shiny <- FALSE }

  invisible(yml)
}
